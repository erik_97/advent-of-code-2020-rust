fn parse(input: &str) -> u16 {
    input
        .chars()
        .rev()
        .enumerate()
        .map(|(i, c)| match c {
            'B' | 'R' => 1 << i,
            'F' | 'L' => 0,
            _ => panic!("unexpected symbol: {}", c as char),
        })
        .sum()
}

fn part1() {
    let max = include_str!("../input.txt").lines().map(parse).max();
    println!("part1: highest seat number is {:?}", max);
}

fn part2() {
    let mut numbers: Vec<u16> = include_str!("../input.txt").lines().map(parse).collect();
    numbers.sort_unstable();
    let seat = numbers
        .as_slice()
        .windows(2)
        .find(|&w| w[1] - w[0] != 1)
        .unwrap()[0]
        + 1;

    println!("part2: your seat number is {:?}", seat);
}

fn main() {
    part1();
    part2();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        assert_eq!(parse("BFFFBBFRRR"), 567);
        assert_eq!(parse("FFFBBBFRRR"), 119);
        assert_eq!(parse("BBFFBBFRLL"), 820);
    }
}
