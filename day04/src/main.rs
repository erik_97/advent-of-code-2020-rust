use parse_display::{Display, FromStr};
use std::collections::{HashMap, HashSet};

#[derive(Display, FromStr, Clone, Copy, PartialEq, Debug)]
#[from_str(regex = "(?P<0>[0-9]{4})")]
struct Year(u64);

#[derive(FromStr, Clone, Copy, PartialEq, Debug)]
#[display("{0}")]
enum Length {
    /// Centimeters (the correct unit)
    #[from_str(regex = "(?P<0>[0-9]{1,3})cm")]
    Cm(u64),
    /// Inches (the incorrect unit)
    #[from_str(regex = "(?P<0>[0-9]{1,3})in")]
    In(u64),
}

#[derive(FromStr, Clone, Copy, PartialEq, Debug)]
#[from_str(regex = "#(?P<0>[0-9]{6})")]
struct Color(u64);

#[derive(Clone, Copy, PartialEq, Debug)]
struct ID<'a>(&'a str);

#[derive(Display, FromStr, Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum FieldTypes {
    #[display("byr")]
    BirthYear,
    #[display("iyr")]
    IssueYear,
    #[display("eyr")]
    ExpirationYear,
    #[display("hgt")]
    Height,
    #[display("hcl")]
    HairColor,
    #[display("ecl")]
    EyeColor,
    #[display("pid")]
    PassportID,
    #[display("cid")]
    CountryID,
}

enum FieldValues<'a> {
    Year(Year),
    Length(Length),
    Color(Color),
    ID(ID<'a>),
}

struct Passport<'a> {
    fields_present: HashSet<FieldTypes>,
    field_values: HashMap<FieldTypes, FieldValues<'a>>,
}

impl<'a> Passport<'a> {
    fn from(s: &str) -> Self {
        let mut present = HashSet::new();
        let mut fields = HashMap::new();
        for x in s.split_whitespace() {
            let mut parts = x.split(':');
            let (key, value) = {
                (
                    parts.next().unwrap().parse::<FieldTypes>().unwrap(),
                    parts.next().unwrap(),
                )
            };
            present.insert(key);
            match key {
                
            }
        }
        Self {
            fields_present: present,
            field_values: fields,
        }
    }
}

fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_parse_year() {
        assert_eq!("1998".parse::<Year>().unwrap(), Year(1998));
    }

    #[test]
    fn test_parse_length() {
        assert_eq!("183cm".parse::<Length>().unwrap(), Length::Cm(183));
        assert_eq!("59in".parse::<Length>().unwrap(), Length::In(59));
    }

    #[test]
    fn test_parse_color() {
        assert!("#113".parse::<Color>().is_err());
        assert_eq!("#123456".parse::<Color>(), Ok(Color(123456)));
        assert!("#12345678".parse::<Color>().is_err())
    }

    #[test]
    fn test_parse_id() {
        assert_eq!("123".into::<ID>(), Ok(ID("123")));
    }
}
