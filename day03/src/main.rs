use parse_display::{Display, FromStr};
use std::ops::AddAssign;

#[derive(Debug, Clone, Copy)]
struct Vec2D {
    x: usize,
    y: usize,
}

impl AddAssign for Vec2D {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

#[derive(Display, FromStr, PartialEq, Debug, Clone, Copy)]
enum Tile {
    #[display("#")]
    Tree,
    #[display(".")]
    Open,
}

#[derive(Debug)]
struct Map {
    width: usize,
    tiles: Vec<Tile>,
}

impl Map {
    fn new(tiles: &str) -> Self {
        Self {
            width: tiles.split('\n').next().expect("No data").len(),
            tiles: tiles
                .chars()
                .filter(|&c| c != '\n')
                .map(|t| t.to_string().parse::<Tile>().unwrap())
                .collect(),
        }
    }

    fn get_tile(&self, pos: Vec2D) -> Option<Tile> {
        self.tiles
            .get(pos.x % self.width + pos.y * self.width)
            .copied()
    }

    fn count_trees(&self, slope: Vec2D) -> usize {
        let mut pos = Vec2D { x: 0, y: 0 };
        let mut count = 0;

        while let Some(tile) = self.get_tile(pos) {
            if let Tile::Tree = tile {
                count += 1;
            }
            pos += slope;
        }

        count
    }
}

fn part1() -> anyhow::Result<()> {
    let file_string = include_str!("../input.txt");
    let map = Map::new(file_string);
    let slope = Vec2D { x: 3, y: 1 };
    let tree_count = map.count_trees(slope);
    println!("part1: {}", tree_count);
    Ok(())
}

fn part2() -> anyhow::Result<()> {
    let file_string = include_str!("../input.txt");
    let map = Map::new(file_string);
    let slopes = vec![
        Vec2D { x: 1, y: 1 },
        Vec2D { x: 3, y: 1 },
        Vec2D { x: 5, y: 1 },
        Vec2D { x: 7, y: 1 },
        Vec2D { x: 1, y: 2 },
    ];
    let product: usize = slopes.iter().map(|&s| map.count_trees(s)).product();
    println!("part2: {}", product);
    Ok(())
}

fn main() -> anyhow::Result<()> {
    part1()?;
    part2()?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_tile_in_bound_at_zero() {
        let map = Map::new("#.#\n#.#\n#.#");
        let pos = Vec2D { x: 0, y: 0 };
        // assert!(map.get_tile(pos).is_some());
        assert_eq!(map.get_tile(pos), Some(Tile::Tree));
    }

    #[test]
    fn test_get_tile_in_bound_middle() {
        let map = Map::new("#.#\n#.#\n#.#");
        let pos = Vec2D { x: 1, y: 1 };
        assert_eq!(map.get_tile(pos), Some(Tile::Open));
    }

    #[test]
    fn test_get_tile_in_bound_at_border() {
        let map = Map::new("#.#\n#.#\n#.#");
        let pos = Vec2D { x: 2, y: 2 };
        assert_eq!(map.get_tile(pos), Some(Tile::Tree));
    }

    #[test]
    fn test_get_tile_in_bound_at_extended() {
        let map = Map::new("#.#\n#.#\n#.#");
        let pos = Vec2D { x: 7, y: 2 };
        assert_eq!(map.get_tile(pos), Some(Tile::Open));
    }

    #[test]
    fn test_get_tile_outside_bound() {
        let map = Map::new("#.#\n#.#\n#.#");
        let pos = Vec2D { x: 2, y: 5 };
        assert!(map.get_tile(pos).is_none());
    }

    #[test]
    fn test_count_trees() {
        let map = Map::new("#.#\n#.#\n#.#\n#.#");
        let slope = Vec2D { x: 1, y: 1 };
        assert_eq!(map.count_trees(slope), 3);
    }
}
