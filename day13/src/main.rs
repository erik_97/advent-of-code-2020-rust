type Bus = (u64, u64);
type Buses = Vec<Bus>;

fn parse_file(s: &str) -> anyhow::Result<(u64, Buses)> {
    let mut s = s.lines();
    let (timestamp, bus_string) = {
        (
            s.next().map(str::parse::<u64>).expect("timestamp")?,
            s.next().expect("bus string"),
        )
    };

    let buses = bus_string
        .split(',')
        .enumerate()
        .filter_map(|(i, c)| c.parse().ok().map(|d| (i as u64, d)))
        .collect::<Vec<(u64, u64)>>();

    Ok((timestamp, buses))
}

fn calculate_id_waiting_time(timestamp: u64, buses: &[Bus]) -> (u64, u64) {
    let (bus_id, minutes_waiting) = buses
        .iter()
        .map(|&(_, bus)| (bus, bus - timestamp % bus))
        .min_by(|(_, x), (_, y)| x.cmp(y))
        .unwrap();

    (bus_id, minutes_waiting)
}

fn part1() -> anyhow::Result<()> {
    let file_string = include_str!("../input.txt");
    let (timestamp, buses) = parse_file(file_string)?;
    let (bus_id, waiting_time) = calculate_id_waiting_time(timestamp, &buses);

    println!("part1: {}", bus_id * waiting_time);

    Ok(())
}

fn inv_mod(a: u64, m: u64) -> u64 {
    // Using Euler's theorem
    // See en.wikipedia.org/wiki/Modular_multiplicative_inverse#Using_Euler's_theorem

    (0..m - 2).fold(1, |c, _| (c * a) % m)
}

fn calculate_first_timestamp(busses: &[(u64, u64)]) -> u64 {
    let prod = busses.iter().fold(1, |acc, (_, i)| acc * i);
    prod - busses
        .iter()
        .map(|&(a, b)| (prod / b) * a * inv_mod(prod / b, b))
        .sum::<u64>()
        .rem_euclid(prod)
}

fn part2() -> anyhow::Result<()> {
    let file_string = include_str!("../input.txt");
    let (_, buses) = parse_file(file_string)?;
    let first_timestamp = calculate_first_timestamp(&buses);

    println!("part2: {}", first_timestamp);

    Ok(())
}

fn main() -> anyhow::Result<()> {
    part1()?;
    part2()?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_file() {
        let input = "123\n4,x,x,x,77,22,9";

        let expected = (123, vec![(0, 4), (4, 77), (5, 22), (6, 9)]);
        assert_eq!(parse_file(input).unwrap(), expected);
    }

    #[test]
    fn test_calculate_id_waiting_time() {
        assert_eq!(
            calculate_id_waiting_time(939, &[(0, 7), (1, 13), (4, 59), (6, 31), (7, 19)]),
            (59, 5)
        );
    }

    #[test]
    fn test_inv_mod() {
        assert_eq!(inv_mod(3, 7), 5)
    }

    #[test]
    fn test_calculate_first_timestamp() {
        assert_eq!(
            calculate_first_timestamp(&[(0, 7), (1, 13), (4, 59), (6, 31), (7, 19)]),
            1068781
        );
    }
}
