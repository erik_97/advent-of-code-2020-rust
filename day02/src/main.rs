use parse_display::{Display, FromStr};

#[derive(Display, FromStr, Debug, PartialEq)]
#[display("{min_count}-{max_count} {required_symbol}")]
struct PasswordPolicy {
    min_count: usize,
    max_count: usize,
    required_symbol: char,
}

impl PasswordPolicy {
    fn is_valid_part_one(&self, password: &str) -> bool {
        ((self.min_count)..=(self.max_count)).contains(
            &password
                .chars()
                .filter(|&b| b == self.required_symbol)
                .count(),
        )
    }

    fn is_valid_part_two(&self, password: &str) -> bool {
        password
            .chars()
            .enumerate()
            .filter(|(p, c)| {
                (*p == self.min_count - 1 || *p == self.max_count - 1) && *c == self.required_symbol
            })
            .count()
            == 1
    }
}

fn parse_line(s: &str) -> anyhow::Result<(PasswordPolicy, &str)> {
    let (policy, password) = {
        let mut tokens = s.split(':');
        (
            tokens.next().expect("policy"),
            tokens.next().expect("password").trim(),
        )
    };

    let policy = policy.parse::<PasswordPolicy>()?;

    Ok((policy, password))
}

fn part1() -> anyhow::Result<()> {
    let count = include_str!("../input.txt")
        .lines()
        .map(parse_line)
        .map(Result::unwrap)
        .filter(|(policy, password)| policy.is_valid_part_one(password))
        .count();

    println!("part1: {} passwords are valid", count);

    Ok(())
}

fn part2() -> anyhow::Result<()> {
    let count = include_str!("../input.txt")
        .lines()
        .map(parse_line)
        .map(Result::unwrap)
        .filter(|(policy, password)| policy.is_valid_part_two(password))
        .count();

    println!("part2: {} passwords are valid", count);

    Ok(())
}

fn main() -> anyhow::Result<()> {
    part1()?;
    part2()?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parsing_single_line() {
        assert_eq!(
            parse_line("2-4 l: greenpenguin").unwrap(),
            (
                PasswordPolicy {
                    min_count: 2,
                    max_count: 4,
                    required_symbol: 'l',
                },
                "greenpenguin",
            )
        );
    }

    #[test]
    fn test_is_valid_part_one_true() {
        let policy = PasswordPolicy {
            min_count: 1,
            max_count: 3,
            required_symbol: 'e',
        };
        assert_eq!(policy.is_valid_part_one("greenpenguin"), true);
        assert_eq!(policy.is_valid_part_one("enola"), true);
    }

    #[test]
    fn test_is_valid_part_one_false() {
        let policy = PasswordPolicy {
            min_count: 1,
            max_count: 3,
            required_symbol: 'e',
        };
        assert_eq!(policy.is_valid_part_one("eternaleverest"), false);
        assert_eq!(policy.is_valid_part_one("bacon"), false);
    }

    #[test]
    fn test_is_valid_part_two_true() {
        let policy = PasswordPolicy {
            min_count: 1,
            max_count: 3,
            required_symbol: 'g',
        };
        assert_eq!(policy.is_valid_part_two("greenpenguin"), true);
    }

    #[test]
    fn test_is_valid_part_two_false() {
        let policy = PasswordPolicy {
            min_count: 8,
            max_count: 10,
            required_symbol: 'v',
        };
        assert_eq!(policy.is_valid_part_two("eternaleverest"), false);
        assert_eq!(policy.is_valid_part_two("bacon"), false);
    }
}
