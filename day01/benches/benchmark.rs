use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion};
use day01::*;

fn bench_fibs(c: &mut Criterion) {
    let mut group = c.benchmark_group("Find pair");

    for input_file in ["input_small.txt", "input.txt"].iter() {
        let input = get_input(input_file).unwrap();

        group.bench_with_input(
            BenchmarkId::new("Recursive 2", input_file),
            input_file,
            |b, _| b.iter(|| find_pair_iterate(black_box(&input), 2020)),
        );

        group.bench_with_input(BenchmarkId::new("Set 2", input_file), input_file, |b, _| {
            b.iter(|| find_pair_set(black_box(&input), 2020))
        });

        group.bench_with_input(
            BenchmarkId::new("Combinations 2", input_file),
            input_file,
            |b, _| b.iter(|| find_pair_combinations(black_box(&input), 2020)),
        );

        group.bench_with_input(
            BenchmarkId::new("Recursive 3", input_file),
            input_file,
            |b, _| b.iter(|| find_triple_iterate(black_box(&input))),
        );

        group.bench_with_input(BenchmarkId::new("Set 3", input_file), input_file, |b, _| {
            b.iter(|| find_triple_set(black_box(&input)))
        });

        group.bench_with_input(
            BenchmarkId::new("Combinations 3", input_file),
            input_file,
            |b, _| b.iter(|| find_triple_combinations(black_box(&input))),
        );
    }

    group.finish();
}

criterion_group!(benches, bench_fibs);
criterion_main!(benches);
