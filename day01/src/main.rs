use day01::*;

fn part1() -> anyhow::Result<()> {
    let numbers = get_input("input.txt")?;
    let (a, b) =
        find_pair_iterate(&numbers, 2020).expect("No pair of numbers with the sum of 2020");
    println!("part1: {}", a * b);
    Ok(())
}

fn part2() -> anyhow::Result<()> {
    let numbers = get_input("input.txt")?;
    let (a, b, c) = find_triple_iterate(&numbers).expect("No pair of numbers with the sum of 2020");
    println!("part2: {}", a * b * c);
    Ok(())
}

fn main() -> anyhow::Result<()> {
    part1()?;
    part2()?;
    Ok(())
}
