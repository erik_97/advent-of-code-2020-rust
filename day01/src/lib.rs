use itertools::Itertools;
use std::collections::HashSet;

pub fn get_input(path: &str) -> anyhow::Result<Vec<i32>> {
    Ok(std::fs::read_to_string(path)?
        .split('\n')
        .map(str::parse::<i32>)
        .collect::<Result<Vec<i32>, _>>()?)
}

// part 1

pub fn find_pair_iterate(numbers: &[i32], target: i32) -> Option<(i32, i32)> {
    for i in 0..numbers.len() {
        for j in 0..numbers.len() {
            if i == j {
                continue;
            }
            if numbers[i] + numbers[j] == target {
                return Some((numbers[i], numbers[j]));
            }
        }
    }
    None
}

pub fn find_pair_set(numbers: &[i32], target: i32) -> Option<(i32, i32)> {
    let numbers: HashSet<i32> = numbers.iter().copied().collect();
    for number in numbers.iter() {
        let missing = target - number;
        if number != &missing && numbers.contains(&missing) {
            return Some((*number, missing));
        }
    }
    None
}

pub fn find_pair_combinations(numbers: &[i32], target: i32) -> Option<(i32, i32)> {
    numbers
        .iter()
        .tuple_combinations()
        .find(|(&a, &b)| a + b == target)
        .map(|(a, b)| (*a, *b))
}

// part 2

pub fn find_triple_iterate(numbers: &[i32]) -> Option<(i32, i32, i32)> {
    for i in 0..numbers.len() {
        for j in 0..numbers.len() {
            for k in 0..numbers.len() {
                if i == j || i == k || j == k {
                    continue;
                }
                if numbers[i] + numbers[j] + numbers[k] == 2020 {
                    return Some((numbers[i], numbers[j], numbers[k]));
                }
            }
        }
    }
    None
}

pub fn find_triple_set(numbers: &[i32]) -> Option<(i32, i32, i32)> {
    for &a in numbers.iter() {
        let missing = 2020 - a;
        if let Some((b, c)) = find_pair_set(&numbers, missing) {
            if a != b && a != c {
                return Some((a, b, c));
            }
        }
    }
    None
}

pub fn find_triple_combinations(numbers: &[i32]) -> Option<(i32, i32, i32)> {
    numbers
        .iter()
        .tuple_combinations()
        .find(|(&a, &b, &c)| a + b + c == 2020)
        .map(|(a, b, c)| (*a, *b, *c))
}

#[cfg(test)]
mod tests {
    use super::*;

    // part 1 tests

    #[test]
    fn test_find_pair_iterate() {
        let input = vec![1721, 979, 366, 299, 675, 1456];
        let result = find_pair_iterate(&input, 2020);

        let expected1 = Some((1721, 299));
        let expected2 = Some((299, 1721));
        assert!(result == expected1 || result == expected2);
    }

    #[test]
    fn test_find_pair_set() {
        let input = vec![1721, 979, 366, 299, 675, 1456];
        let result = find_pair_set(&input, 2020);

        let expected1 = Some((1721, 299));
        let expected2 = Some((299, 1721));
        assert!(result == expected1 || result == expected2);
    }

    #[test]
    fn test_find_pair_combinations() {
        let input = vec![1721, 979, 366, 299, 675, 1456];
        let result = find_pair_combinations(&input, 2020);

        let expected1 = Some((1721, 299));
        let expected2 = Some((299, 1721));
        assert!(result == expected1 || result == expected2);
    }

    // part 2 tests

    #[test]
    fn test_find_triple_iterate() {
        let input = vec![1721, 979, 366, 299, 675, 1456];
        let result = find_triple_iterate(&input);

        let expected = vec![
            Some((366, 675, 979)),
            Some((366, 979, 675)),
            Some((675, 979, 366)),
            Some((979, 675, 366)),
            Some((675, 366, 979)),
            Some((979, 366, 675)),
        ];
        assert!(expected.contains(&result));
    }

    #[test]
    fn test_find_triple_set() {
        let input = vec![1721, 979, 366, 299, 675, 1456];
        let result = find_triple_set(&input);

        let expected = vec![
            Some((366, 675, 979)),
            Some((366, 979, 675)),
            Some((675, 979, 366)),
            Some((979, 675, 366)),
            Some((675, 366, 979)),
            Some((979, 366, 675)),
        ];
        assert!(expected.contains(&result));
    }

    #[test]
    fn test_find_triple_combinations() {
        let input = vec![1721, 979, 366, 299, 675, 1456];
        let result = find_triple_combinations(&input);

        let expected = vec![
            Some((366, 675, 979)),
            Some((366, 979, 675)),
            Some((675, 979, 366)),
            Some((979, 675, 366)),
            Some((675, 366, 979)),
            Some((979, 366, 675)),
        ];
        assert!(expected.contains(&result));
    }
}
