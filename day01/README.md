## Benchmarks

### Part 1

| input file      | Iterate   | Set       | Combinations |
|-----------------|-----------|-----------|--------------|
| input_small.txt | 4.2620 ns | 181.00 ns | 5.2730 ns    |
| input.txt       | 7.6074 us | 4.9372 us | 8.2365 us    |

### Part 2

| input file      | Iterate   | Set       | Combinations |
|-----------------|-----------|-----------|--------------|
| input_small.txt | 34.927 ns | 472.76 ns | 44.760 ns    |
| input.txt       | 845.68 us | 198.27 us | 1.4289 ms    |